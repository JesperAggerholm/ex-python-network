#!/usr/bin/env python3
import socket
import random
import time

def readSensors():
    # sensor "readings" are just a random number
    sensorReadings = str('''Hello, is it this text string your looking 
for?''')
    return sensorReadings

def init_client_socket( host, port ):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((host, port))

    return s

def send_data( sock, data ):
    sock.sendall(data.encode('utf-8'))

if __name__ == "__main__":
    server_address = '127.0.0.1'      # The server's hostname or IP address
    port = 65432                      # The port to send data to on the server
    max_readings = 2       # the number of readings to send

    print( "Connecting to '%s' on port '%d'"%( server_address, port) )
    s = init_client_socket( server_address, port)

    for i in range(1, max_readings):
        mySensorReadings = readSensors()
        send_data(s, mySensorReadings)
        time.sleep( 1 )
